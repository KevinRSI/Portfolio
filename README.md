# Projet

Création et réalisation d'un projet de portfolio pour le site simplonlyon.fr contenant une présentation de soi ainsi que nos réalisations et une page de contact.

## Réalisation

On commence par une création de wireframe pour avoir une idée approximative du design final du site :

[![Wireframe.png](/img/Wireframe.png)](https://whimsical.com/portfolio-CUpKwqdHR5V1Pyx2thdcpa)

Ensuite vient la réalisation du site en utilisant HTML/CSS avec Bootstrap et un peu de JS pour animer quelques élements.

une fois le code terminé on le valide avec W3C Validator pour vérifier si il y'a des erreurs dans le html et Wave pour tester l'accesibilité.

## Mise en ligne

[Lien du site](https://kevinrsi.gitlab.io/Portfolio/)
